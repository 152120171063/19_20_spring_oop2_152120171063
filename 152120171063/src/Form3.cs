﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO; //

namespace PreLab
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FileStream file = new FileStream("info.csv", FileMode.Append); //Append değişikler yapmayı sağlar
            StreamWriter streamWrite = new StreamWriter(file);
            streamWrite.Write(textBox1.Text + ";" + User.getHashSha256(textBox2.Text) + "\n");
            streamWrite.Close();
            file.Close();

            this.Hide();            
        }

        private void button2_Click(object sender, EventArgs e) //
        {
            SaveFileDialog save = new SaveFileDialog();
            save.Filter = "TSV Dosyası|*.tsv";
            save.OverwritePrompt = true;
            save.CreatePrompt = true;

            if (save.ShowDialog() == DialogResult.OK)
            {
                StreamWriter record = new StreamWriter(save.FileName);
                record.Write(textBox1.Text);
                record.Write("\t");
                record.Write(User.getHashSha256(textBox2.Text) + "\n");
                record.Close();
            }
        }    
    }
}