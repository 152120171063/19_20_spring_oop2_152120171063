﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO; //

namespace PreLab
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string name, password;
        private void button1_Click(object sender, EventArgs e)
        {
            var list = new List<string>();
            var fileStream = new FileStream("info.csv", FileMode.Open, FileAccess.Read);
            using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
            {
                string line;
                while ((line = streamReader.ReadLine()) != null)
                {
                    list.Add(line);
                }
            }
            fileStream.Close();

            char[] chars = { ' ', ';' };
            string words = string.Join(";", list);
            string[] word = words.Split(chars);
            int tmp = 0;
            for (int i = 0; i < word.Count(); i++)
            {
                if (word[i] == textBox1.Text)
                {
                    tmp = i;
                }                    
                name = word[tmp];
                password = word[tmp + 1];
            }
            for (int i = 0; i < word.Count(); i++)
            {
                if ((textBox1.Text == name) && (User.getHashSha256(textBox2.Text) == password))
                {
                    label3.Text = "SUCCESS!";
                    label3.ForeColor = Color.Green;
                    timer1.Enabled = true;
                }
                else
                {
                    label3.Text = "FAILURE";
                    label3.ForeColor = Color.Red;
                    textBox1.Clear();
                    textBox2.Clear();
                }
            }            

            if (checkBox1.Checked == true)
            {
                Properties.Settings.Default.UserName = textBox1.Text;
                Properties.Settings.Default.Password = textBox2.Text;
                Properties.Settings.Default.Save();
            }

            if (checkBox1.Checked == false)
            {
                Properties.Settings.Default.UserName = "";
                Properties.Settings.Default.Password = "";
                Properties.Settings.Default.Save();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.Hide(); //şu an üzerinde çalıştığım formu sakla(hide)
            Form2 newForm = new Form2(); //yeni bi form2 oluştur
            newForm.Show(); //bu yeni form2 yi göster
            timer1.Enabled = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {                       
            Form3 newForm = new Form3();
            newForm.Show();                                   
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.UserName != string.Empty)
            {
                textBox1.Text = Properties.Settings.Default.UserName;
                textBox2.Text = Properties.Settings.Default.Password;
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
        }
    }
}