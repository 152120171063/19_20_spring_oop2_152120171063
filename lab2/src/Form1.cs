﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PreLab2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Singleton obj = Singleton.getSingleton();


            if ((obj.name[0].Equals(textBox1.Text) && obj.password1.Equals(Singleton.getHashSha256(textBox2.Text))) || (obj.name[1].Equals(textBox1.Text) && obj.password2.Equals(Singleton.getHashSha256(textBox2.Text))))
            {
                label3.Text = "SUCCESS!";
                label3.ForeColor = Color.Green;
                timer1.Enabled = true;
            }
            else
            {
                label3.Text = "FAILURE!";
                label3.ForeColor = Color.Red;
                textBox1.Clear();
                textBox2.Clear();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.Hide(); //şu an üzerinde çalıştığım formu sakla(hide)
            Form2 newForm = new Form2(); //yeni bi form2 oluştur
            newForm.Show(); //bu yeni form2 yi göster
            timer1.Enabled = false;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
    }
}