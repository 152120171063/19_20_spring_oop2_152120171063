﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;


namespace PreLab2
{
    class Singleton //Temelinde nesneleri birden çok kez oluşturulmasındansa birkez oluşturup 
                    //sonraki aşamalarda bu oluşturulan nesnenin örneğini kullanmamızı sağlayan desen.
    {

        public static string getHashSha256(string text)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(text);
            SHA256Managed hashstring = new SHA256Managed();
            byte[] hash = hashstring.ComputeHash(bytes);
            string hashString = string.Empty;
            foreach (byte x in hash)
            {
                hashString += String.Format("{0:x2}", x);
            }
            return hashString;
        }

        public string[] name = { "pinar", "kzlrsln" };
        public string password1 = getHashSha256("12345");
        public string password2 = getHashSha256("asdf");

        private Singleton()
        {

        }
        private static Singleton obj;
        public static Singleton getSingleton()
        {
            if (obj == null)
            {
                obj = new Singleton();
            }
            return obj;
        }
    }
}